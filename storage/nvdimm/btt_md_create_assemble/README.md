# storage/nvdimm/btt_md_create_assemble

Storage: nvdimm create and assemble raid using mdadm with btt

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
