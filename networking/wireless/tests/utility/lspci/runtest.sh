#!/bin/bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Author: Laura Trivelloni <ltrivell@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2023 Red Hat, Inc. All rights reserved.
#
#   RedHat Internal.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
if [ -f /usr/lib/beakerlib/beakerlib.sh ]
then
    . /usr/lib/beakerlib/beakerlib.sh
elif [ -f /usr/share/beakerlib/beakerlib.sh ]
then
    . /usr/share/beakerlib/beakerlib.sh
fi

rlJournalStart
    rlPhaseStartTest
    	local lspci_output
    	lspci_output=$(rlRun "lspci -vnn" 0 "lspci")
    	rlLogInfo "$lspci_output"

        rlRun "lspci -vnn | grep \"Network controller\"" 0 "Get network controller info"
    rlPhaseEnd
rlJournalEnd
rlJournalPrintText
